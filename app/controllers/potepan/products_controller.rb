class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_LIMIT_MAX_NUMBER = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxons = @product.taxons
    @related_products = @product.related_products.
      includes(master: [:images, :default_price]).
      limit(RELATED_PRODUCTS_LIMIT_MAX_NUMBER)
  end

  def search
    @search_word = params[:searchWord]
    searched_products = Spree::Product.ransack(name_or_description_cont: params[:searchWord]).
      result.includes(variants_including_master: %i(images prices))
    @products = searched_products
  end
end
