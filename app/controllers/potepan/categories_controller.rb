class Potepan::CategoriesController < ApplicationController
  helper_method :count_number_of_products_by_option
  def show
    @categories = Spree::Taxonomy.all.includes(:taxons)
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.all_products.includes(master: [:images, :default_price]).from_newest_to_oldest.sort_in_order(params[:sort])
    @colors = Spree::OptionValue.values_all("Color")
    @sizes = Spree::OptionValue.values_all("Size")
    @sorts = Spree::OptionValue.values_all("Sort")
  end

  def filter_params
    { color: params[:color], size: params[:size], sort: params[:sort] }
  end

  def count_number_of_products_by_option(option_value)
    @taxon.all_products.includes(variants: :option_values).where(spree_option_values: { name: option_value.name }).count
  end
end
