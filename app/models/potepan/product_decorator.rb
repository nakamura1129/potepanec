module Potepan::ProductDecorator
  Spree::Product.class_eval do
    def related_products
      Spree::Product.in_taxons(taxons).where.not(id: id).distinct
    end
    scope :filter_by_color, -> (color) { joins(variants: :option_values).where(potepan_option_values: { name: color }).distinct }
    scope :filter_by_size, -> (size) { joins(variants: :option_values).where(potepan_option_values: { name: size }).distinct }

    scope :from_high_price_to_low_price, -> { unscope(:order).descend_by_master_price }
    scope :from_low_price_to_high_price, -> { unscope(:order).ascend_by_master_price }
    scope :from_newest_to_oldest, -> { reorder(available_on: :desc) }
    scope :from_oldest_to_newest, -> { reorder(available_on: :asc) }

    scope :sort_in_order, -> (sort) do
      case sort
      when "NEW_PRODUCTS"
        from_newest_to_oldest
      when "OLD_PRODUCTS"
        from_oldest_to_newest
      when "LOW_PRICE"
        from_low_price_to_high_price
      when "HIGH_PRICE"
        from_high_price_to_low_price
      end
    end
  end
  Spree::Product.prepend self
end
