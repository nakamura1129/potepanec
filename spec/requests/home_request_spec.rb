require 'rails_helper'

RSpec.describe "HomeRequests", type: :request do
  describe "#index" do
    let!(:new_products) { create_list(:product, 8, available_on: 2.day.ago) }
    let(:latest_product) { create(:product, available_on: 1.day.ago) }

    before do
      get potepan_path
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    # index.html.erbが描画されているか
    it "index.html.erbが表示されること" do
      expect(response).to render_template :index
    end

    it "@new_productsが渡されていること" do
      expect(assigns(:new_products)).to match_array new_products[0..7]
    end
  end
end
