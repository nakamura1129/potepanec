require 'rails_helper'

RSpec.describe "CategoriesRequests", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "HTTPリクエストが成功する" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリ名が表示される" do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include taxon.name
    end

    it "categories変数の確認" do
      expect(assigns(:categories)).to contain_exactly(taxonomy)
    end

    it "taxon変数の確認" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "product変数の確認" do
      expect(assigns(:products)).to contain_exactly(product)
    end
  end
end
