require 'rails_helper'

RSpec.describe "ProductsRequests", type: :request do
  describe "GET #show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create(:product, name: "MUG", taxons: [taxon]) }
    let!(:product_else) { create(:product, name: "TOTE", taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(id: product.id)
    end

    it "HTTPリクエストが成功する" do
      expect(response).to have_http_status(200)
    end

    it "関連商品が4つ表示されること" do
      expect(assigns(:related_products).count).to eq 4
    end

    it "関連していない商品が含まれていないこと" do
      expect(response).not_to include "TOTE"
    end
  end
end
