require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  describe 'カテゴリーページ' do
    let!(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let!(:taxon_1) { create(:taxon, name: 'Bags', taxonomy: taxonomy) }
    let!(:taxon_2) { create(:taxon, name: 'Mugs', taxonomy: taxonomy) }
    let!(:product_1) { create(:product, name: 'RUBY ON RAILS TOTE', taxons: [taxon_1]) }
    let!(:product_2) { create(:product, name: 'RUBY ON RAILS MUG', taxons: [taxon_2]) }
    let!(:option_types) do
      [
        create(:option_type, presentation: "Color"),
        create(:option_type, presentation: "Size"),
      ]
    end
    let!(:colors) do
      [
        create(:option_value, name: "Red", presentation: "Red", option_type: option_types[0]),
        create(:option_value, name: "Blue", presentation: "Blue", option_type: option_types[0]),
      ]
    end
    let!(:sizes) do
      [
        create(:option_value, name: "Small", presentation: "S", option_type: option_types[1]),
        create(:option_value, name: "Medium", presentation: "M", option_type: option_types[1]),
      ]
    end
    let!(:variants) do
      [
        create(:variant, option_values: [colors[0]]),
        create(:variant, option_values: [colors[1]]),
        create(:variant, option_values: [sizes[0]]),
        create(:variant, option_values: [sizes[1]]),
      ]
    end

    before do
      visit potepan_category_path(taxon_1.id)
    end

    it "カテゴリーの名前と商品の名前と価格が表示されていること" do
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(product_1.display_price)
      expect(page).to have_content(product_1.name)
    end

    context 'product_1の場合' do
      it '商品一覧ページから商品詳細ページに遷移すること' do
        expect(page).to have_content 'RUBY ON RAILS TOTE'
        click_on 'RUBY ON RAILS TOTE'
        expect(current_path).to eq potepan_product_path(product_1.id)
      end
    end

    it '色フィルターをかけらること' do
      click_link colors[0].presentation
      expect(page).to have_content(product_1.name)
      expect(page).not_to have_content(product_2.name)
    end

    scenario "新着順になること" do
      select("新着順", from: "sort")
      expect(page).to have_select("sort", selected: "新着順")
    end
    scenario "古い順になること" do
      select("古い順", from: "sort")
      expect(page).to have_select("sort", selected: "古い順")
    end
    scenario "高い順になること" do
      select("高い順", from: "sort")
      expect(page).to have_select("sort", selected: "高い順")
    end
    scenario "安い順になること" do
      select("安い順", from: "sort")
      expect(page).to have_select("sort", selected: "安い順")
    end
  end
end
