require 'rails_helper'

RSpec.describe '商品詳細ページ', type: :system do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "GET #show" do
    before do
      visit potepan_product_path(id: product.id)
    end

    it "商品名が表示される" do
      expect(page).to have_content product.name
    end

    it "商品価格が表示される" do
      expect(page).to have_content product.display_price
    end

    it "商品説明が表示される" do
      expect(page).to have_content product.description
    end
  end

  describe "関連商品" do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(id: product.id)
    end

    it "同カテゴリーの関連商品が表示される" do
      within(".productsContent") do
        expect(page).to have_content related_products.name
        expect(page).to have_content related_products.display_price
      end
    end

    it "関連商品がリンクを持っている" do
      expect(page).to have_link href: potepan_product_path(related_products.id)
      click_link href: potepan_product_path(related_products.id)
      expect(current_path).to eq potepan_product_path(related_products.id)
    end
  end
end
