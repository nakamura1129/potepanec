require 'rails_helper'

RSpec.describe "Home", type: :system do
  let!(:products) { create_list(:product, 8) }

  it "新着商品" do
    visit potepan_path
    within(".featuredProducts") do
      expect(page).to have_link products.first.name, href: potepan_product_path(products.first.id)
      expect(page).to have_content products.first.display_price
    end
  end
end
